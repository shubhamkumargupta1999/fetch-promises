// 1. Fetch all the users

function fetchAllUsers() {
    fetch("https://jsonplaceholder.typicode.com/users")
        .then((data) => {
            return data.json();
        })
        .then((users) => {
            console.log(users);
        })
}
// 2. Fetch all the todos

function fetchAllTodos() {
    fetch("https://jsonplaceholder.typicode.com/todos")
        .then((data) => {
            return data.json();
        })
        .then((todos) => {
            console.log(todos);
        })
}

// 3. Use the promise chain and fetch the users first and then the todos.

function fetchUsersThenTodos() {
    fetch("https://jsonplaceholder.typicode.com/users")
        .then((data) => {
            return data.json();
        })
        .then((users) => {
            console.log(users);

            return fetch("https://jsonplaceholder.typicode.com/todos")
        })
        .then((data) => {
            return data.json();
        })
        .then((todos) => {
            console.log(todos);
        })
}

// 4. Use the promise chain and fetch the users first and then all the details for each user.

function fetchUsersAndDetails() {
    fetch("https://jsonplaceholder.typicode.com/users")
        .then((data) => {
            return data.json();
        })
        .then((users) => {
            console.log(users);

            return users;
        })
        .then((users) => {
            users.forEach((curr) => {
                console.log(curr);
            })
        })
}
// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

function fetchFirstTodoAndUserDetails() {
    fetch("https://jsonplaceholder.typicode.com/todos?id=1")
        .then((data) => {
            return data.json();
        })
        .then((todo) => {
            console.log(todo);
            return fetch(`https://jsonplaceholder.typicode.com/users?id=${todo[0].userId}`)
        })
        .then((data) => {
            return data.json()
        })
        .then((user) => {
            console.log(user);
        })
}

fetchFirstTodoAndUserDetails();